# passthroughbrowser

Waydroid Passthrough Browser - Open links in host browser

Passthrough Browser has two components --- a server and a client. The server is
a Python program written with Flask, while the client is an Android app written
using [MIT App Inventor](https://appinventor.mit.edu). The server listens for
HTTP POST connections on TCP port 8888 on an IP address only accessible to
programs running on the host or in the Waydroid container. When it receives a
properly formatted request, it opens the host's browser to the URL specified in
the request. The client, when installed in Waydroid, is intended to be set as
the default browser. After this is done, whenever an Android app attempts to
open a Web page, it will open the Passthrough Browser client just as it would
launch any other browser. The client then sends the URL to the server, which
causes the host browser to open.

## Installation

Run `./install_both.sh`. (Make sure Waydroid is running first.) Installing the
client requires running a command as root; the script uses `sudo`. You may need
to enter your password during installation.

## Credits

The client app uses the
[CustomWebView](https://github.com/vknow360/CustomWebView/tree/beta) (beta
version) App Inventor extension by Sunny Gupta. CustomWebView is used under the
MIT licese.
