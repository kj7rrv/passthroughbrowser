#!/bin/bash
echo "Running 'sudo waydroid app install PassThroughBrowser.apk'. Please enter your"
echo "password if prompted:"
sudo waydroid app install PassThroughBrowser.apk
cat << HERE
To set up the Passthrough Browser client:
  1. Open Settings in Waydroid.
  2. Click/tap "Apps and Notifications."
  3. Click/tap "Default apps."
  4. Click/tap "Browser app."
  5. Click/tap "Passthrough Browser."
HERE
