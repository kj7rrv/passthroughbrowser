#!/bin/bash
mkdir -p ~/.config/autostart
mkdir -p ~/.local/bin/
cat > ~/.config/autostart/passthrough_browser_server.desktop << HERE
[Desktop Entry]
Type=Application
Name=Passthrough Browser Server
Exec=$HOME/.local/bin/passthrough_browser_server
HERE
cp server.py ~/.local/bin/passthrough_browser_server
echo "To start the Passthrough Browser server, log out and log back in. (Wait until"
echo "you have set the client up, if it isn't already installed.)"
