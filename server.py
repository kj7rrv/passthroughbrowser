#!/usr/bin/env python3
from flask import Flask
from flask import request, Response
import webbrowser
import time

app = Flask(__name__)

@app.route('/', methods = ['GET', 'POST'])
def page():
    if request.method == 'GET':
        return """\
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Passthrough Browser Server</title>
    </head>
    <body>
        <h1>Passthrough Browser Server</h1>
        <p>This is the server for <a href=https://git.kj7rrv.com/kj7rrv/passthroughbrowser>Passthrough Browser</a>. Passthrough Browser is an Android app that, when set as the default browser in Waydroid, causes links to open in the host computer's browser.</p>
        <p>If you have not yet installed the client app in Waydroid, follow these steps to install it:</p>
        <ul>
            <li>Open Waydroid</li>
            <li>Open a terminal</li>
            <li><code>cd</code> to the directory where you downloaded Passthrough Browser</li>
            <li><code>./install_client.sh</code></li>
            <li>Follow the instructions on the command line</li>
        </ul>
    </body>
</html>
"""
    elif request.method == 'POST':
        webbrowser.open(request.form['url'])
        return Response('', 203)

if __name__ == '__main__':
    while True:
        try:
            app.run(host='192.168.250.1', port=8888)
        except OSError as e:
            print(e)
            time.sleep(3)

